# PROJECTSETUP

To make this real damn easy no matter your background as I have not converted this to a super easy project-app-base setup installer as of yet.

1. [PROJECTSETUP](PROJECTSETUP)
   1. [Preparation](#Preparation)
   2. [Pubspec](#Pubspec)
   


## Preparation

At this point, you still have to copy certain things manually. Those items are:

- dot-editorconfig file

- dot-gitignore file

- anaylsis_options.yaml file

- dartdocs_options.yaml file

- derry.yaml file

  You will change the hard-wired urls for derry script nodes of 
  test, mygoldens, uml, and docs.

- skinparams.iuml file

for these folders:

- dot-run folder copy the whole folder and contents over

  You will have one Android Studio or IntelliJ run configuration   
  to adjust as it uses absolute urls to point to a file.

- dot-vscode folder copy the whole folder and contents over

  No changes needed to contents as loading project folder 
  autosets up VSCode with all the settings and prompts you to 
  load the suggested plugins for the workspace.

- images folder and contents over

- integration_test and contents over

  Delete the imports and re-assign imports.

- lib folder and contents over

  Delete imports and re-assign imports.

- test folder and contents except png and goldens folder over

  Delete imports and re-assign imports.

- test_driver folder and contents over

  Delete imports and re-assign imports.

## Pubspec

You will add this to pubspec:

under dependencies:
```
meta: ^1.3.0
golden_toolkit: ^0.8.0

```
 under dev_dependencies

```
integration_test: ^1.0.2+2
build_runner: ^1.11.1
dart_code_metrics: ^2.4.0
mockito: ^4.1.4

```

under the flutter block

```
assets:
    - images/

```

And at the very end of the pubspec file

```
# required for derry scripts
scripts: derry.yaml

```

