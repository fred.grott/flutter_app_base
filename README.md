# Flutter_App_Base

![git-project-card](./images/git_project_card.png)

This is a flutter app demo project to show the right app base of code you should be starting with to get the maximum amount of deliberate practice towards flutter app development mastership.

<!-- Shields -->
[![Twitter](https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/fredgrott)[![Xing](https://img.shields.io/badge/Xing-006567?style=for-the-badge&logo=xing&logoColor=white)](https://www.xing.com/profile/Fred_Grott/cv)[![keybase](https://img.shields.io/badge/Keybase-33A0FF?&style=for-the-badge&logo=keybase&logoColor=white)](https://keybase.io/fredgrott)[![gitlab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/fred.grott)[![medium](https://img.shields.io/badge/medium-%2312100E.svg?&style=for-the-badge&logo=medium&logoColor=white)](https://fredgrott.medium.com)[![linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/fredgrottstartupfluttermobileappdesigner/)![dart](https://img.shields.io/badge/dart-%230175C2.svg?&style=for-the-badge&logo=dart&logoColor=white)![flutter](https://img.shields.io/badge/Flutter%20-%2302569B.svg?&style=for-the-badge&logo=Flutter&logoColor=white)![vscode](https://img.shields.io/badge/VSCode-007ACC?&style=for-the-badge&logo=visual-studio-code&logoColor=white)![android studio](https://img.shields.io/badge/Android_Studio-3DDC84?&style=for-the-badge&logo=android-studio&logoColor=white)![markdown](https://img.shields.io/badge/Markdown-000000?&style=for-the-badge&logo=markdown&logoColor=white)![bsdlicense](https://img.shields.io/badge/-BSD_License-61DAFB?&logoColor=white&style=for-the-badge)

<!-- Using live badgen shield list from https://badgen.net/ -->
![Starrers](https://badgen.net/gitlab/stars/fred.grott/flutter_app_base)![Forks](https://badgen.net/gitlab/forks/fred.grott/flutter_app_base)![Open Issues](https://badgen.net/gitlab/open-issues/fred.grott/flutter_app_base)

- [Flutter_App_Base](#flutter_app_base)
  - [Pre-requisites](#pre-requisites)
  - [Getting Started With Flutter](#getting-started-with-flutter)
  - [About The Project](#about-the-project)
  - [Usage](#usage)
  - [License](#license)
  - [Contact](#contact)
  - [Resources](#resources)
  - [Why You Should Follow Me](#why-you-should-follow-me)

## Pre-requisites

You need to install LCOV-GENHTML and Python JUNIT2HTML besides just GIT. AND, at least one Flutter IDE of either Android Studio or VSCode.

## Getting Started With Flutter

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)
- [Flutter Community-Where Devs answer questions](https://flutter.dev/community)

For help to get started with Flutter, view Flutter's [online documentation](<https://flutter.dev/docs>), which offers tutorials, samples, guidance on mobile development, and a full API reference.

## About The Project

![screenshot](./images/screenshot.png)

This flutter app base project demos the perfect app base setup for starter flutter app development. It includes the derry workflow script to semi-automate project workflow.

## Usage

You need to install the DartDoc, Derry, DCDG, Dart_Code_Metrics, JUNITREPORT, DHTTPD, and DEVTOOLS dart binaries to use this flutter project.

After installing those dart binaries you can clone this maser or the forks to use with:

```bash
git clone https://gitlab.com/fred.grott/flutter_app_base.git
```

Or even easier, download the git repo and follow the directions in the projectsetup readme.

Then open the project in your Flutter IDE and modify.

## License

Distributed under the BSD License. See [LICENSE](https://gitlab.com/fred.grott/flutter_app_base/-/blob/master/LICENSE) for more information.

## Contact

email: fred DOT grott AT gmail DOT com

## Resources

I am writing several flutter application development books and my development articles on flutter wil be found here that match up with the code content of this git repo.

## Why You Should Follow Me

There is a load of information online about Flutter, but it's scattered into non-curated bits-n-pieces.
I am curating the flutter development information into core technique articles that then are being re-worked into chapters in several flutter development books I am writing.

But, that is not all! I am creating and preparing a series of flutter demo app git projects that will form the core of al the coe examples both min my medium articles and my flutter dev books. They are always highlighted in my medium articles.

Even though giving your expert flutter techniques all-at-once will feel like drinking-from-a-firehose I do believe that my curating the techniques into any easy sequence of deliberate practices will get-up-to-speed in mastering both flutter app development and getting that first flutter app development job.

Remember, pretend in a git project repo does not work anymore. It's not just code and visual design; it's stepping you into a set of very specific thinking modes and models combined with stepping you through how to reach people through communication.

And together, through hard work, we are together going to get you hired as a Flutter App Developer.
