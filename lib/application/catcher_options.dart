// Copyright(c) 2021 Fredrick Allan Grott. All rights reserved.
// Use of this source code is governed by a BSD-style license.

String errorWidgetTitle = "Error Report";
String errorWidgetDescription = "Found this group of errors:";
