// Copyright(c) 2021 Fredrick Allan Grott. All rights reserved.
// Use of this source code is governed by a BSD-style license.



import 'package:fimber/fimber.dart';

String myAppTag = "Flutter_App_Base";

FimberLog logger = FimberLog(myAppTag);

Fimber plantMeDebug() {
  Fimber.plantTree(DebugTree.elapsed(useColors: true));

  return Fimber();
}


