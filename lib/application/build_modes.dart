// Copyright(c) 2021 Fredrick Allan Grott. All rights reserved.
// Use of this source code is governed by a BSD-style license.

bool get isInDebugMode {
  bool _inDebugMode = false;

  return _inDebugMode ? (_inDebugMode = true) : (_inDebugMode = false);
}

bool get isInReleaseMode {
  bool _inReleaseMode = false;

  return _inReleaseMode ? (_inReleaseMode = true) : (_inReleaseMode = false);
}

bool get isInProfileMode {
  bool _inProfileMode = false;

  return _inProfileMode ? (_inProfileMode = true) : (_inProfileMode = false);
  
}
