// Copyright(c) 2021 Fredrick Allan Grott. All rights reserved.
// Use of this source code is governed by a BSD-style license.

import 'dart:async';


import 'package:ansicolor/ansicolor.dart';
import 'package:catcher/catcher.dart';
import 'package:catcher/handlers/console_handler.dart';
import 'package:catcher/handlers/email_manual_handler.dart';
//import 'package:catcher/handlers/http_handler.dart';
import 'package:catcher/handlers/toast_handler.dart';
import 'package:catcher/mode/dialog_report_mode.dart';
import 'package:catcher/mode/page_report_mode.dart';
import 'package:catcher/model/catcher_options.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app_base/application/build_modes.dart';
import 'package:flutter_app_base/application/my_log_setup.dart';

import 'package:flutter_app_base/presentation/my_app.dart';
//import 'package:sentry/sentry.dart';

// In Domain Driven Development terms you would move the other
// widget presentation classes to the presentation sub-folder
Future<void> main() async {
  // Many times we have to call something to initialize before the Sky Engine is
  // fully in-place or before runApp is called as runApp internally does call
  // WidgetsFlutterBinding.ensureInitialized. An example is initializing Firebase client
  // call stuff.
  WidgetsFlutterBinding.ensureInitialized();

  plantMeDebug();
  logger.d("Test message", ex: Exception("test error"));

  final CatcherOptions debugOptions = CatcherOptions(DialogReportMode(), [
      //EmailManualHandler([]),
      ToastHandler(),
      ConsoleHandler(),
      //HttpHandler()
  ],localizationOptions: [
    LocalizationOptions(
      "en",
      dialogReportModeTitle: "App Error",
      dialogReportModeDescription: "Sorry for the error",
      dialogReportModeAccept: "YES",
      dialogReportModeCancel: "NO",
    ),]);
  final CatcherOptions releaseOptions = CatcherOptions(PageReportMode(), [
      //SentryHandler(
      //SentryClient(SentryOptions(dsn: 'YOUR DSN HERE')),
      //printLogs: true,
    //)
    EmailManualHandler(["recipient@email.com"])
  ]);

  

  FlutterError.onError = (FlutterErrorDetails details) async {
    if (isInDebugMode) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // app exceptions provider. We do not need this in Profile mode.
      if (isInReleaseMode) {
        Zone.current.handleUncaughtError(details.exception, details.stack);
      }
    }
  };

  // Most medium sized flutter apps are asynchronous and so we need to run it in
  // at least one Dart Zone.
  runZonedGuarded<Future<void>>(
    () async {
      Catcher(
      runAppFunction: () {
        runApp(MyApp());
      },
      debugConfig: debugOptions,
      releaseConfig: releaseOptions,ensureInitialized: true);

      // ignore: prefer-trailing-comma
    },
    (error, StackTrace stackTrace) async {
      assert(stackTrace != null);
      await _reportError(error, stackTrace);
    },
    zoneSpecification: ZoneSpecification(
      // Intercept all print calls
      print: (self, parent, zone, line) async {
        // Paint all logs with Cyan color
        final pen = AnsiPen()..cyan(bold: true);
        // Include a timestamp and the name of the App
        final messageToLog = "[${DateTime.now()}] MyApp $line";

        // Also print the message in the "Debug Console"
        parent.print(zone, pen(messageToLog));
      },
    ),
  );
}

Future<void> _reportError(dynamic error, dynamic stackTrace) async {
  logger.d('Caught error: $error');
  // Errors thrown in development mode are unlikely to be interesting. You
  // check if you are running in dev mode using an assertion and omit send
  // the report.
  if (isInDebugMode) {
    logger.d('$stackTrace');
    logger.d('In dev mode. Not sending report to an app exceptions provider.');

    return;
  } else {
    // reporting error and stacktrace to app exceptions provider code goes here
    if (isInReleaseMode) {
      // code goes here
    }
  }
}
